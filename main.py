import collections
import flask
import logging
import ast
import urllib.parse
import other
logging.basicConfig(filename='server.log',level=logging.INFO)
app = flask.Flask(__name__)
@app.route("/request/<request_graph>")
def hello(request_graph): 
    logging.error(request_graph)
    request_graph = other.unquote(request_graph)
    logging.error(request_graph)
    return str(request_graph)

if __name__ == '__main__':
    #graph = {0: [1, 2], 1: [3], 2: [3], 3: [1, 2]}
    #bfs(graph, 0)
    app.run(debug=True)
