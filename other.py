import urllib.parse
import collections
import ast
def bfs(graph, number):
    result=[]
    graph=ast.literal_eval(graph)
    went, now = set(), collections.deque([number])
    went.add(number)
    while now:
        now_now = now.popleft()
        result.append((str(now_now)))
        for neighbor in graph[now_now]:
            if neighbor not in went:
                went.add(neighbor)
                now.append(neighbor)
    return result

def unquote(text):
    return(bfs(urllib.parse.unquote_plus(
        str(text)), 1))


print((unquote("%7B0:%20[1,%202],%201:%20[3],%202:%20[3],%203:%20[1,%202]%7D")))
